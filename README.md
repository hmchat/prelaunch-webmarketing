# Harmony Pre-Launch Website  
  
## Mission  
  
- Announce the presence of the project.
- Announce the purpose of the project.
- Announce the main features of the project.
- Clearly state the mission of the project.  
- Clearly state the roadmap of the project.  
- Clearly state the ways that will be used to accomplish the mission of the project using the roadmap.

*This list may be incomplete and may be amended in the future.*
  
## Design  

The design language of the pre-launch website may not be reflective of the final project's design language and/or user interface/experience, including key points such as elements, typefaces, products, product names, layouts and the logo.

## Build  

You can start a development server by running `npm run dev` and build a production-ready website using `npm run build`. You may start the newly-built website using `npm run start` or by deploying the appropriate directory.
    
## Contributing  
  
Anyone can contribute to this open-source project licensed under the Boost Software License, available in the `LICENSE` file of this code repository. If this README is distributed without the entire source code, you can read the license at https://www.boost.org/LICENSE_1_0.txt.
If you have found a security vulnerability (such as XSS), you should directly contact the authors using their public emails before making the issue public.
